import React from 'react';
import Modal from 'react-modal';
import ModalBase from './ModalBase';
import PulseLoader from 'react-spinners/PulseLoader';

class Control extends ModalBase {

  constructor( props ) {
    super( props );
    this.state = { ...this.state, loading:false, ops:[], numbers:[], result:null, status:'', time:0 };
    this.fire = this.fire.bind( this );
    this.handleOpsChange = this.handleOpsChange.bind( this );
    this.handleNumChange = this.handleNumChange.bind( this );
  }
  
  fire( e ) {
    this.setState( { loading:'visible', result:'', time:0 } );
    const s = Date.now();
    e.preventDefault();
    fetch( window.hostUrl + 'multi/' + this.state.ops.join( '_' ), { method:'post', body:JSON.stringify( { numbers:this.state.numbers } ), headers:{ 'Content-Type':'application/json; charset=UTF-8' } } )
      .then( resp => resp.text() )
      .then( json => this.setState( { loading:false, result:json, status:'', time:Date.now() - s } ) )
      .catch( err => this.setState( { loading:false, result:JSON.stringify( err ), status:'uk-alert-warning' } ) );
  }

  handleNumChange( e ) {
    this.setState( { numbers:e.currentTarget.value.trim().split( /\D+/g ).map( s => parseInt( s ) ) } );
  }

  handleOpsChange( e ) {
    const v = e.currentTarget.value;
    const checked = e.currentTarget.checked;
    this.setState( state => {
      let ops = state.ops;
      if( checked ) ops.push( v );
      else ops = ops.filter( e => e !== v );
      return { ops:ops };
    } );
  }

  render() {
    const ops = { mul:'x Multiply', sum:'+ Summarize', str:'convert to String', pow:'^2 power 2' };

    return <div><div className="uk-card uk-card-default">
      <div className="uk-card-header"><h3 className="uk-card-title" onClick={this.handleOpen} style={{cursor:'pointer'}}>Controls</h3></div>
      <div className="uk-card-body">
        <i>click me!</i>
        <Modal isOpen={this.state.show} onRequestClose={this.handleClose} className="Modal" overlayClassName="Overlay">
          <div className="uk-modal-header">
            <button type="button" className="uk-modal-close-default" data-uk-close onClick={this.handleClose}/>
            <h3>Test console</h3>
          </div>
          <div className="uk-modal-body">
            <form onSubmit={this.fire}>
              <input type="text" onChange={this.handleNumChange} placeholder="Enter numbers"/>
              {Object.keys( ops ).map( k => <div key={k}>
                <label><input type="checkbox" value={k} onChange={this.handleOpsChange}/> {ops[ k ]}</label>
              </div>)}
              <input type="submit" value="fire!"/> 
            </form>
            <hr/>
            <div className={'uk-text-center uk-alert ' + this.state.status}>
              <div>{this.state.result}</div>
              <PulseLoader loading={this.state.loading} color="#bbb" sizeUnit="em" size=".6"/>
              {!this.state.loading && this.state.result && <div style={{color:'#aaa'}}>completed in {this.state.time} ms</div>}
            </div>
          </div>
        </Modal>
      </div>
    </div></div>;
  }

}

export default Control;