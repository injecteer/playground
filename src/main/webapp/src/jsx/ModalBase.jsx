import React from 'react';

class ModalBase extends React.Component {

  constructor( props ){
    super( props );
    this.state = { show:false };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen(){
    this.setState( { show:true } );
  }

  handleClose(){
    this.setState( { show:false } );
  }

  render() {
    return null;
  }
}

export default ModalBase;