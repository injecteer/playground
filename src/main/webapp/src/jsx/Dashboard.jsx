import React from 'react';
import Box from './Box'
import Control from './Control';
import Journal from './Journal';

import 'uikit/dist/css/uikit.min.css';
import 'uikit/dist/js/uikit.min.js';
import './Dashboard.css'

class Dashboard extends React.Component {

  render() {
    const range = Array( window.boxCount ).fill().map( (_, ix) => ix );

    return <div className="main">
      <div data-uk-grid="masonry:true" className="uk-padding" >
        <Control/>
        {range.map( ix => <Box key={ix} ix={ix}/> )}
      </div>
      <Journal/>
    </div>;
  }
}

export default Dashboard;