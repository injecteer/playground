import React from 'react';
import Modal from 'react-modal';
import ModalBase from './ModalBase';

class Box extends ModalBase {

  render() {
    const range = Array( Math.ceil( 5 + Math.random() * 10 ) ).fill().map( (_, ix) => ix );

    return <div><div className="uk-card uk-card-default">
      <div className="uk-card-header"><h3 className="uk-card-title">Item {this.props.ix + 1}</h3></div>
      <div className="uk-card-body">
        <div onClick={this.handleOpen} style={{cursor:'pointer'}}>show</div>
        <Modal isOpen={this.state.show} onRequestClose={this.handleClose} className="Modal" overlayClassName="Overlay">
          <div className="uk-modal-header">
            <button type="button" className="uk-modal-close-default" data-uk-close onClick={this.handleClose}/>
            <h3>Modal {this.props.ix}</h3>
          </div>
          <div className="uk-modal-body">
            {range.map( ix => <div key={ix}>
              <div>line {ix}</div>
            </div>)}
          </div>
        </Modal>
      </div>
    </div></div>;
  }
}

export default Box;