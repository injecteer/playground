import React from 'react';
import EventBus from 'vertx3-eventbus-client';

class Journal extends React.Component {

  constructor( props ) {
    super( props );
    this.state = { journal:[], counter:0, msg:'', color:'hsl(' + Math.trunc( Math.random() * 360 ) + ', 100%, 30%)' };
    this.send = this.send.bind( this );
    this.handleChange = this.handleChange.bind( this );
  }

  componentDidMount() {
    if( window.eventBus && 1 === window.eventBus.state ) return;

    const self = this;

    var eb = new EventBus( window.hostUrl + 'eventbus', { vertxbus_ping_interval:60000 } );
    eb.enableReconnect( true );
    eb.onopen = () => {
      eb.registerHandler( 'dashboard', ( err, msg ) => {
        if( msg.init ){
          let j = msg.init.journal;
          self.setState( { counter:msg.init.count, journal:Object.keys( j ).sort().map( d => 
            <div key={d} style={{color:j[ d ].color}}>{new Date( parseInt( d ) ).toLocaleTimeString()} / {j[ d ].message}</div> 
          ) } );
        }else
          self.setState( (prevState) => {
            let j = prevState.journal;
            j.push( <div key={msg.body.now} style={{color:msg.body.color}}>{new Date( msg.body.now ).toLocaleTimeString()} / {msg.body.message}</div> );
            return { counter:msg.body.count, journal:j };
          } );
      } );
    };
    eb.onclose = () => window.eventBus = null;
    window.eventBus = eb;
  }
  
  send( e ) {
    e.preventDefault();
    if( window.eventBus && 1 === window.eventBus.state && 0 !== this.state.msg.length ){
      window.eventBus.send( 'journal', { message:this.state.msg, color:this.state.color } );
      this.setState( { msg:'' } );
    } 
  }

  handleChange( e ) {
    this.setState( { msg:e.target.value.trim() } );
  }

  render() {
    return <div className="journal">
      <h3>Journal</h3>
      <form onSubmit={this.send}>
        <div className="colorBox" style={{backgroundColor:this.state.color}}></div>
        <input type="text" onChange={this.handleChange} value={this.state.msg} placeholder="Enter message"/>
        <input type="submit" value="send"/>
      </form>
      <div> # {this.state.counter}</div>
      <div>
        {this.state.journal}
      </div>
    </div>;
  }
}

export default Journal;