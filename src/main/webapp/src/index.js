import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import Dashboard from './jsx/Dashboard';

Modal.setAppElement( '#root' );

ReactDOM.render(<Dashboard/>, document.getElementById('root'));
