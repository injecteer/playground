vertx.eventBus().consumer( "stringifier", function( message ){
  console.log( "StringifyWorkerVerticle", message.body());
  var strings = message.body().numbers.map( function( s ){ return '' + s } );
  message.reply( { strings:strings } );
} );
