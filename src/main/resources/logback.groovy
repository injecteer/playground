import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender

// See http://logback.qos.ch/manual/groovy.html for details on configuration

appender( 'STDOUT', ConsoleAppender ) {
  encoder( PatternLayoutEncoder ){ pattern = '%d{HH:mm:ss.SSS} [%thread] %level %logger{36}%X{adapter} - %msg%n' }
}
root INFO, [ 'STDOUT' ]
//logger 'com.hazelcast.kubernetes', DEBUG