package io.playground;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

public class MultiplyVerticle extends AbstractVerticle {

  private final Logger log = LoggerFactory.getLogger( MultiplyVerticle.class );

  @Override
  public void start() throws Exception {
    vertx.eventBus().<JsonObject>consumer( "multiplier", msg -> {
      try{
        int sum = msg.body().getJsonArray( "numbers" ).stream().map( v -> (int)v ).reduce( ( acc, curr ) -> acc * curr ).get();
        JsonObject res = new JsonObject().put( "multiplication", sum );
        log.info( ">> " + res );
        msg.reply( res );
      }catch( Exception e ){
        msg.fail( 400, e.getMessage() );
      }
    } );
  }

}
