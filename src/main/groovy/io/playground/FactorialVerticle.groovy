package io.playground

import groovy.util.logging.Log4j
import io.vertx.core.AbstractVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpServerRequest
import io.vertx.core.json.Json

@Log4j
class FactorialVerticle extends AbstractVerticle {

  @Override
  void start() throws Exception {
    vertx.eventBus().<List<Long>>consumer( 'factorial' ){ Message<List<Long>> msg ->
      def ( res, curr ) = msg.body()
      log.info "<< $res / $curr"
      if( 1 >= curr ) 
        msg.reply res
      else 
        vertx.eventBus().send( 'factorial', [ res * curr, curr - 1 ] ){ AsyncResult<Message> reply ->
          reply.succeeded() ? msg.reply( reply.result().body() ) : msg.fail( 500, reply.cause() )
        }
    }
    
    vertx.createHttpServer().requestHandler( { HttpServerRequest req ->
      try{
        long curr = req.path()[ 1..-1 ].toLong()
        vertx.eventBus().send( 'factorial', [ 1l, curr ] ){ AsyncResult<Message> reply ->
          reply.succeeded() ? req.response().end( Json.encodeToBuffer( factorial:reply.result().body() ) ) : req.response().setStatusCode( reply.cause().failureCode ).end( reply.cause().message )
        }
      }catch( Exception e ){
        req.response().setStatusCode 500 end e.message
      }
    } ).listen 8092
  }
}
