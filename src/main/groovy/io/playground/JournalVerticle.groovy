package io.playground

import groovy.transform.TypeChecked
import groovy.util.logging.Log4j
import io.vertx.core.AbstractVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.eventbus.Message
import io.vertx.core.shareddata.AsyncMap
import io.vertx.core.shareddata.Counter

/**
 * Gets the messages over <b>journal</b> consumer, updates shared data and publishes messages to <b>dashborad</b> address.
 * @author injec
 *
 */
@Log4j
@TypeChecked
class JournalVerticle extends AbstractVerticle {

  static final int MAX_SIZE = 20
  
  @Override
  void start() throws Exception {
    vertx.eventBus().<Map>consumer( 'journal' ){
      Map m = it.body()
      if( !m ) return
      log.info "<< $m"
      
      long now = System.currentTimeMillis()
      
      Future<Void> mapPutFut = Future.<Void>future()
      vertx.sharedData().getAsyncMap( 'journal' ){ AsyncResult<AsyncMap<Long,Map>> mapGetRes ->
        if( !mapGetRes.succeeded() ){
          mapPutFut.fail mapGetRes.cause()
          return
        }
        
        AsyncMap<Long,Map> map = mapGetRes.result()
        
        map.size{ AsyncResult<Integer> sizeRes ->
          if( sizeRes.succeeded() ){
            if( MAX_SIZE <= sizeRes.result() ){
              log.info 'map too big, truncating'
              map.keys{ if( it.succeeded() && it.result() ) map.remove it.result().min(), {} }
            }
          }
        }
        
        map.put now, m, mapPutFut
      }
      
      Future<Long> countFut = Future.<Long>future()
      vertx.sharedData().getCounter( 'journalCount' ){ AsyncResult<Counter> counterRes ->
        if( counterRes.succeeded() )
          counterRes.result().incrementAndGet countFut
        else
          countFut.fail counterRes.cause()
      }
      
      CompositeFuture.all( mapPutFut, countFut ).setHandler{ AsyncResult allRes ->
        if( allRes.succeeded() ){
          log.info ">> ${m + [ count:countFut.result() ]}"
          vertx.eventBus().publish 'dashboard', m + [ now:now, count:countFut.result() ]
        }else
          log.warn "noooo! ${allRes.cause()}"
      }
    }
  }
}
