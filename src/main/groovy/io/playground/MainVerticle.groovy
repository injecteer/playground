package io.playground

import static groovy.transform.TypeCheckingMode.SKIP

import groovy.transform.TypeChecked
import groovy.util.logging.Log4j
import io.vertx.core.AbstractVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.CompositeFuture
import io.vertx.core.DeploymentOptions
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.json.Json
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.sockjs.SockJSHandler

@Log4j
@TypeChecked
class MainVerticle extends AbstractVerticle {

  private final Map<String,String> MAPPING = [ sum:'summarizer', mul:'multiplier', str:'stringifier', pow:'power2' ]

  private final DeploymentOptions OPTS = new DeploymentOptions()
  private final DeploymentOptions OPTS_2_INST = [ instances:2 ] as DeploymentOptions
  
  private final DeploymentOptions WORKER_OPTS = [ worker:true, workerPoolSize:4 ] as DeploymentOptions

  private final GroovyTemplateEngine engine = new GroovyTemplateEngine()

  void start() throws Exception {
    Router router = Router.router vertx

    router.route().handler BodyHandler.create()
    router.route '/static/*' handler StaticHandler.create()
    
    // just bark something back
    router.get '/hello' handler{ it.response().end 'helleu sir!' }
    
    // hack to show proper favicon
    router.route '/favicon.ico' handler{ it.response().sendFile 'webroot/favicon.ico' }

    // happy path
    router.post '/mul' consumes 'application/json' produces 'application/json' handler{ RoutingContext rc ->
      vertx.eventBus().send 'multiplier', rc.bodyAsJson, this.&handleRes.curry( rc )
    }
    
    // use of default it
    router.post '/pow' consumes 'application/json' produces 'application/json' handler{
      vertx.eventBus().send 'power2', it.bodyAsJson, this.&handleRes.curry( it )
    }
    
    // sleepy code to demonstrate blocking warning
    router.post '/str' consumes 'application/json' produces 'application/json' handler{
      sleep 3000
      vertx.eventBus().send 'stringifier', it.bodyAsJson, this.&handleRes.curry( it )
    }
    
    // remedied blocking
    router.post '/sum' consumes 'application/json' produces 'application/json' blockingHandler{
      log.info "<< $it.bodyAsString"
      sleep 3000
      vertx.eventBus().send 'summarizer', it.bodyAsJson, this.&handleRes.curry( it )
    }
    
    // now SockJSBridge
    router.route '/eventbus/*' handler sockJS()
    
    //regex request mapping
    router.postWithRegex( /\/multi\/\w+/ ).produces 'application/json' handler this.&multi
    
    // render index.html template with optional param
    router.get '/:boxCount' handler this.&showIndex
    // ... or without it
    router.get '/' handler this.&showIndex
    
    vertx.createHttpServer().requestHandler router listen 8090

    vertx.deployVerticle JournalVerticle, OPTS
    
    // deploy MultiplyV with 2 instances to demonstrate round-robin
    vertx.deployVerticle MultiplyVerticle.name, OPTS_2_INST
    vertx.deployVerticle FactorialVerticle, OPTS_2_INST
    
    // the rest are worker verticles
    vertx.deployVerticle SumarizeWorkerVerticle, WORKER_OPTS
    vertx.deployVerticle 'js/io/playground/StringifyWorkerVerticle.js', WORKER_OPTS
    vertx.deployVerticle 'scala:io.playground.Power2WorkerVerticle', WORKER_OPTS
  }

  /**
   * renders index.html with params
   * @param rc
   */
  @TypeChecked( SKIP )
  private void showIndex( RoutingContext rc ) {
    engine.render( '/index.html', [ boxCount:rc.pathParam( 'boxCount' )?.toInteger() ?: 10 ] ){ AsyncResult<Buffer> res ->
      res.succeeded() ? rc.response().end( res.result() ) : rc.response().setStatusCode( 400 ).end( res.cause().message )
    }
  }
  
  @TypeChecked( SKIP )
  private SockJSHandler sockJS() {
    def opts = [ pingTimeout:70_000, inboundPermitteds:[ [ addressRegex:/\w+/ ] ], outboundPermitteds:[ [ addressRegex:/\w+/ ] ] ]
    SockJSHandler.create vertx bridge opts, new SockJSBridge( vertx:vertx )
  }

  /**
   * sends multiple requests to event bus, joins the replies and sends them back over http-response
   * @param rc
   */
  private void multi( RoutingContext rc ){
    String path = rc.request().path().substring 7
    log.info "<< $path / $rc.bodyAsJson"
    List<Future<Map>> futs = (List)MAPPING.findResults{ k, addr ->
      if( !path.contains( k ) ) return null
      Future<Map> f = Future.<Map>future()
      vertx.eventBus().send( addr, rc.bodyAsJson ){ AsyncResult<Message<Map>> res ->
        res.succeeded() ? f.complete( res.result().body() ) : f.fail( res.cause() )
      }
      f
    }
    if( futs )
      CompositeFuture.join( futs ).handler = { AsyncResult<Map> res ->
        Map all = futs.inject( [:] ){ acc, curr -> curr.succeeded() ? ( acc << curr.result() ) : acc }
        rc.response().end Json.encodeToBuffer( all )
      }
    else
      rc.response().setStatusCode 204 end()
  }
  
  private void handleRes( RoutingContext rc, AsyncResult<Message<Map>> reply ) {
    log.info ">> ${reply.result()?.body()} / ${reply.cause()} / ${reply.result()?.body()?.getClass()}"
    if( reply.succeeded() )
      rc.response().end Json.encodeToBuffer( reply.result().body() )
    else{
      int code = ((ReplyException)reply.cause()).failureCode
      rc.response().setStatusCode code end '{"message":"' + reply.cause().message + '"}'
    }
  }
}
