package io.playground

import groovy.transform.TypeChecked
import groovy.util.logging.Log4j
import io.vertx.core.AsyncResult
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.json.Json
import io.vertx.core.shareddata.AsyncMap
import io.vertx.ext.bridge.BridgeEventType
import io.vertx.ext.web.handler.sockjs.BridgeEvent

@Log4j
@TypeChecked
class SockJSBridge implements Handler<BridgeEvent> {

  Vertx vertx
  
  @Override
  void handle( BridgeEvent be ) {
    log.info "be << ${be.type()} / $be.rawMessage"
    if( BridgeEventType.REGISTER != be.type() ){
      be.complete true
      return
    }
    
    Future<Map> mapFut = Future.<Map>future()
    vertx.sharedData().getAsyncMap( 'journal' ){ AsyncResult<AsyncMap> mapGetRes ->
      if( mapGetRes.succeeded() )
        mapGetRes.result().entries mapFut
      else
        mapFut.fail mapGetRes.cause()
    }
    
    Future<Long> countFut = Future.<Long>future()
    vertx.sharedData().getCounter( 'journalCount' ){
      it.succeeded() ? it.result().get( countFut ) : countFut.fail( it.cause() )
    }
    
    CompositeFuture.all mapFut, countFut setHandler{
      if( it.succeeded() ){
        be.socket().write Json.encodeToBuffer( address:'dashboard', init:[ journal:mapFut.result(), count:countFut.result() ] )
        be.complete true
      }else
        be.fail it.cause()
    }
    
  }

}
