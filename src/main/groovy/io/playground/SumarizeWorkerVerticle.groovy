package io.playground

import groovy.util.logging.Log4j
import io.vertx.core.AbstractVerticle
import io.vertx.core.eventbus.Message

@Log4j
class SumarizeWorkerVerticle extends AbstractVerticle {

  @Override
  void start() throws Exception {
    vertx.eventBus().<Map>consumer( 'summarizer' ){ Message<Map> msg ->
      log.info "<< ${msg.body()}"
      try{
        sleep 3000
        msg.reply sum:msg.body().numbers.sum()
      }catch( Exception e ){
        msg.fail 400, e.message
      }
    }
  }
}
