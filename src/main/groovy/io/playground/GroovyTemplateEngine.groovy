package io.playground

import static io.vertx.core.Future.*

import java.util.concurrent.ConcurrentHashMap

import groovy.text.StreamingTemplateEngine
import groovy.text.Template
import groovy.transform.TypeChecked
import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.buffer.Buffer

@TypeChecked
class GroovyTemplateEngine {

  private final ConcurrentHashMap<String, Template> cache
  
  private StreamingTemplateEngine engine = new StreamingTemplateEngine()
  
  GroovyTemplateEngine( boolean doCache = true ) {
    cache = doCache ? new ConcurrentHashMap() : null
  }

  void render( String templateFileName, LinkedHashMap<String,?> context, Handler<AsyncResult<Buffer>> handler ) {
    try{
      Template template = isCachingEnabled() ? cache[ templateFileName ] : null
      if( !template ) synchronized( this ){
        this.getClass().getResource( templateFileName ).withReader{
          template = engine.createTemplate it
          if( isCachingEnabled() ) cache[ templateFileName ] = template
        }
      }
      BufferWriter out = new BufferWriter()
      out << template.make( context )
      handler?.handle succeededFuture( out.buf )
    }catch( Exception e ){ 
      handler?.handle failedFuture( e ) 
    }
  }
  
  boolean isCachingEnabled() { null != cache }
  
  private class BufferWriter extends Writer {
    
    Buffer buf = Buffer.buffer()
    
    @Override
    void write( char[] cbuf, int off, int len ) throws IOException {
      buf.appendString new String( cbuf, off, len )
    }

    @Override void close() throws IOException {}
    @Override void flush() throws IOException {}
  }
}
