package io.playground

import scala.math.pow

import org.slf4j.LoggerFactory

import io.vertx.core.json.JsonObject
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.lang.scala.json.Json
import io.vertx.scala.core.eventbus.Message

object Power2WorkerVerticle {
  
  def replyPower( json:JsonObject ) = {
    val numbers = json getJsonArray( "numbers" )
    val seq = for{
      ix <- 0 until numbers.size
      num = pow( numbers.getDouble( ix ), 2 ).toInt
    } yield num
    Json obj( ( "power2", seq.toList ) )
  }
  
}

class Power2WorkerVerticle extends ScalaVerticle {
  
  private val log = LoggerFactory.getLogger( Power2WorkerVerticle.getClass.getName )
  
  override def start() {
    vertx.eventBus.consumer[JsonObject]( "power2", ( msg:Message[JsonObject] ) => {
      val res = Power2WorkerVerticle.replyPower( msg.body )
      log.info( s">> ${res}" )
      msg.reply( res )
    } )
  }
  
}