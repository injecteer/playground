package io.playground

import io.vertx.core.Vertx
import io.vertx.core.eventbus.ReplyException
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.AsyncConditions
import spock.util.concurrent.PollingConditions

class MainVerticleSpec extends Specification {
  
  @Shared Vertx vertx
  
  @Shared AsyncConditions conditions = new AsyncConditions()
  
  def setupSpec() {
    vertx = Vertx.vertx()
    vertx.deployVerticle( MainVerticle.name ){ res -> conditions.evaluate{ assert res.succeeded() } }
    conditions.await 2
  }
  
  def "test EB multiplier consumer"(){
    setup:
    def msg = [ numbers:[ 1, 2, 3, 4 ] ]
    
    expect:
    vertx.eventBus().send( 'multiplier', msg ){ res ->
      assert res.succeeded()
      conditions.evaluate{
        assert res.result().body() == [ multiplication:24 ]
      }
    }
    
    conditions.await 5
  }
  
  def "test EB multiplier consumer wrong numbers"(){
    setup:
    def msg = [ numbers:[ 1, 2, 'aaa' ] ]
    
    expect:
    vertx.eventBus().send( 'multiplier', msg ){ res ->
      assert !res.succeeded()
      conditions.evaluate{
        assert res.cause().class == ReplyException
        assert res.cause().message == 'java.lang.String cannot be cast to java.lang.Integer'
      }
    }
    
    conditions.await 5
  }
  
  def cleanupSpec() {
    vertx.close{ res -> conditions.evaluate{ assert res.succeeded() } }
    conditions.await 2
  }
  
}
