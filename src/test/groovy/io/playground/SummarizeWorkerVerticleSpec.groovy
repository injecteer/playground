package io.playground

import io.vertx.core.Vertx
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.PollingConditions

class SummarizeWorkerVerticleSpec extends Specification {
  
  @Shared Vertx vertx
  
  PollingConditions conditions = new PollingConditions( timeout:4 )
  
  def setupSpec() {
    vertx = Vertx.vertx()
    vertx.deployVerticle( SumarizeWorkerVerticle.name, [ worker:true ] ){ assert it.succeeded() }
    sleep 1000
  }
  
  def "test EB summarizer consumer"(){
    given:
    def msg = [ numbers:[ 1, 2, 3, 4 ] ]
    def result
    
    when:
    vertx.eventBus().send( 'summarizer', msg ){
      assert it.succeeded()
      result = it.result().body()
    }
    
    then:
    conditions.eventually{
      assert result == [ sum:10 ]
    }
  }
  
  def cleanupSpec() {
    sleep 1000
    vertx.close()
  }
  
}
