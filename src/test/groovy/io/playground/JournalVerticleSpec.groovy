package io.playground

import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.shareddata.AsyncMap
import io.vertx.core.shareddata.Counter
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.AsyncConditions
import spock.util.concurrent.PollingConditions

class JournalVerticleSpec extends Specification {
  
  @Shared Vertx vertx
  
  def setupSpec() {
    vertx = Vertx.vertx()
    AsyncConditions conditions = new AsyncConditions()
    vertx.deployVerticle( MainVerticle.name ){ res -> conditions.evaluate{ assert res.succeeded() } }
    conditions.await 2
  }
  
  def "test simple send"() {
    given:
    AsyncConditions conditions = new AsyncConditions()
    AsyncConditions sharedDataCond = new AsyncConditions()
    AsyncConditions counterCond = new AsyncConditions()

    vertx.eventBus().<Map>consumer( 'dashboard' ){ Message<Map> message ->
      conditions.evaluate{
        assert 'test' == message.body().message 
        assert 1 == message.body().count
      }
    }
    
    when:
    vertx.eventBus().send 'journal', [ message:'test' ]
    vertx.eventBus().send 'journal', [ message:'test' ] // fire another to check size increase
    
    then:
    conditions.await 5
    
    vertx.sharedData().getAsyncMap( 'journal' ){ AsyncResult<AsyncMap> mapGetRes ->
      mapGetRes.result().size{ sizeRes -> sharedDataCond.evaluate{ assert 2 == sizeRes.result() } }
    }
    vertx.sharedData().getCounter( 'journalCount' ){ AsyncResult<Counter> mapCntRes ->
      mapCntRes.result().get{ sizeRes -> counterCond.evaluate{ assert 2 == sizeRes.result() } }
    }
    
    sharedDataCond.await 2
    counterCond.await 2
  }
  
  def "test journal overflow"() {
    setup:'get initial count 1st, to neglect previous test cases'
    AsyncConditions initialCounterCond = new AsyncConditions()
    int initCount
    vertx.sharedData().getCounter( 'journalCount' ){ AsyncResult<Counter> cntGetRes ->
      cntGetRes.result().get{ cntRes -> initialCounterCond.evaluate{ assert null != ( initCount = cntRes.result() ) } }
    }
    initialCounterCond.await 2
    
    when:'run send 40 times'
    40.times{ 
      vertx.eventBus().send 'journal', [ message:'test' ] 
      sleep 1
    }
    
    then:'check if map size is 20 and counter = initCount + 40'
    AsyncConditions sharedDataCond = new AsyncConditions()
    vertx.sharedData().getAsyncMap( 'journal' ){ AsyncResult<AsyncMap> mapGetRes ->
      mapGetRes.result().size{ sizeRes -> sharedDataCond.evaluate{ assert 20 == sizeRes.result() } }
    }
    sharedDataCond.await 6
    
    AsyncConditions counterCond = new AsyncConditions()
    vertx.sharedData().getCounter( 'journalCount' ){ AsyncResult<Counter> cntGetRes ->
      cntGetRes.result().get{ cntRes -> counterCond.evaluate{ assert 40 + initCount == cntRes.result() } }
    }
    counterCond.await 6
  }
  
  def cleanupSpec() {
    AsyncConditions conditions = new AsyncConditions()
    vertx.close{ res -> conditions.evaluate{ assert res.succeeded() } }
    conditions.await 2
  }
  
}
