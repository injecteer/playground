A demo Vert.x project showing

- use of Event Bus
- http REST endpoints
- use of SockJS server/client
- Groovy verticle
- Java verticle
- Javascript verticle
- Scala verticle
- ReactJS integration
- async composition options
- verticle deployment options
- avaliable gradle tasks

Simply import the project into your IDE and use `run` or `runShadow` commandos.